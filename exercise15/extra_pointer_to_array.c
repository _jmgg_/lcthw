#include <stdio.h>
/*
 * Messing around the internet I found the declaration
 * of pointers to arrays, here I mess a bit and try understanding
 * why arr2 prints the same value as *(arr2).
 */
int main(int argc, char *argv[])
{
    int arr[3] = {0, 0, 7};
    int (*arr2)[3] = &arr;

    printf("&arr2 : %p -> %p \n", &arr2, *(arr2));
    printf("arr2 : %p | *arr2 : %p \n", arr2, *(arr2));
    printf("%p : %d \n", arr2, *(*(arr2)));

    return 0;
}
