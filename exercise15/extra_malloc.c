/* 
 * Rewrite all arrays as pointers, this extra credit was vague to interpret
 * so here I try "literally" to convert the usage of arrays to pointers by
 * obtaining a pointer to a chunk of memory of size of the array
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    // create two arrays we care about

    int *ages = malloc(sizeof(int)*5);
    *(ages) = 23;
    *(ages + 1) = 24;
    ages[2] = 25;
    *(ages + 3) = 26;
    *(ages + 4) = 27;

    int i = 0;

    for (i = 0; i < 5; i++) {
        printf("%p : %d\n", ages+i, *(ages + i));
    }

    free(ages);

    return 0;
}
