# Exercise 17 | Heap and Stack memory allocation

In this exercise we create a fake database. I've used this README to explain a
bit some things I've done for some of the points in "How to break it" and
"Extra credit"

## How to break it

### `strncpy` bug and how to fix it

Usage of `strncpy` is prone to bugs because if we pass a >512 bytes (chars)
name or email we will end up not copying the nul byte (\0) and thus when
printing the address we will see that it prints past the original string.

To fix this we can do various things:

* Manually add the terminator to the end of the name/email array

    In this case all we need to do is manually set the last element of the
name and email array/buffer to `\0`

    ```c
    char *dup = strdup(name);
    // Take care of off-by-one errors
    dup[MAX_DATA-1] = '\0';
    char *res = strncpy(addr->name, dup, MAX_DATA);
    ```
* Use `strlcpy` instead of `strncpy` as it is safer and guarantees to
      NUL-terminate the result. Basically doing it for us. Take into account
    that in order to use this bsd library in some systems you will probably need to
    add `-lbsd` to your CFLAGS  to work

    ```c
    #include <bsd/string.h>

    size_t
    strlcpy(char *dst, const char *src, size_t size);
    // In our code
    int copied = strlcpy(addr->name, name, MAX_DATA);
    ```  

    This way, although name being larger than `MAX_DATA`, `strlcpy`
NUL-terminates correctly as it copies up to size-1 leaving the last byte as NUL

## Extra credit

### Improve `die`

We want to be able to pass the current Connection to `die` so we can free stuff
graciously before exiting.

We just need to add a `struct Connection` pointer parameter, only to be used
after the `if-else` block for freeing stuff by `Database_close`
```
void die(const char *message, struct Connection *conn)
{
    if(errno) {
        perror(message);
    } else {
        printf("ERROR: %s\n", message);
    }

    Database_close(conn);

    exit(1);
}
```

