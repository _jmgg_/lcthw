#include <stdio.h>

int main(int argc, char *argv[])
{
    int areas[] = {10, 12, 13, 14, 20};
    char name[] = "Zed";
    char full_name[] = {
        'Z', 'e', 'd', ' ', 'A', '.', ' ', 'S', 'h', 'a', 'w', '\0'
    };

    areas[12] = 27;
    printf("The size of an int: %ld\n", sizeof(int));
    printf("The size of areas (int[]): %ld\n", sizeof(areas));
    printf("Number of ints in areas: %ld\n", sizeof(areas) / sizeof(int));
    printf("The first area is: %d, 2nd: %d.\n", areas[12], areas[1]);

    printf("The size of a char: %ld\n", sizeof(char));
    printf("The size of name (char[]): %ld\n", sizeof(name));
    printf("Number of char in name: %ld\n", sizeof(name) / sizeof(char));
    
    printf("The size of full name (char[]): %ld\n", sizeof(full_name));
    printf("The number of chars: %ld\n", sizeof(full_name) / sizeof(char));

    printf("name=\"%s\" and full_name=\"%s\"\n", name, full_name);

    // EXTRA CREDIT

    // Try assigning to elements in the areas array
    areas[0] = 100;
    areas[1] = -1;
    areas[12] = 42;
    printf("areas[0] = %d, areas[1] = %d, areas[12] = %d\n", areas[0], areas[1], areas[12]);

    // Try assigning to elements of name and full_name
    name[0] = 'S';
    full_name[0] = 'S';
    printf("name[0] = %c, full_name[0] = %c\n", name[0], full_name[0]);

    // Try setting one elem from areas to a char from name
    // Possible explanation: chars and int are both stored as bytes, so compiler is ok.
    areas[2] = name[0];
    printf("areas[2] = %c | name[0] = %c\n", areas[2], name[0]);

    return 0;
}
